import React, { useState } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CourseInfo from './components/CourseInfo/CourseInfo';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseForm from './components/CourseForm/CourseForm';
import { PrivateRoute } from './components/PrivateRoute/PrivateRoute';

function App() {
	const [allCourses, setAllCourses] = useState([]);
	const [allAuthors, setAllAuthors] = useState([]);
	const [name, setName] = useState('');

	const handleSaveCourse = (newCourse) => {
		setAllCourses((prevCourses) => [...prevCourses, newCourse]);
	};

	return (
		<>
			<Header name={name} />
			<Routes>
				<Route path='*' element={<Navigate to='/login' />} />
				<Route path='/login' element={<Login />} />
				<Route path='/registration' element={<Registration />} />
				<Route path='/courses' element={<Courses />} />
				<Route
					path='/courses/:courseId'
					element={
						<CourseInfo allCourses={allCourses} allAuthors={allAuthors} />
					}
				/>
				<Route
					path='/courses/update/:courseId'
					element={
						<PrivateRoute>
							<CourseForm />
						</PrivateRoute>
					}
				/>
				<Route
					path='/courses/add'
					element={
						<PrivateRoute>
							<CourseForm
								setAllCourses={handleSaveCourse}
								setAllAuthors={setAllAuthors}
							/>
						</PrivateRoute>
					}
				/>
			</Routes>
		</>
	);
}

export default App;
