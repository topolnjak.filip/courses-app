import { coursesSuccess } from './store/courses/actions';
import { authorsSuccess } from './store/authors/actions';
export const BASE_URL_API = 'http://localhost:4000';

export const loginUser = async (userData) => {
	try {
		const result = await fetch(`${BASE_URL_API}/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
			},
			body: JSON.stringify(userData),
		});

		if (!result.ok) {
			const data = await result.json();
			console.error('Login failed:', data.error);
			throw new Error('Login failed');
		}

		const data = await result.json();
		console.log('Login successful:');
		const token = data.result;
		const userName = data.user?.name;

		if (!token) {
			throw new Error('Token is missing in response');
		}

		localStorage.setItem('token', token);
		localStorage.setItem('user', userName || '');

		return { userName, token };
	} catch (error) {
		console.error('Unexpected response during login:', error);
		throw error;
	}
};

export const getUserData = async (token) => {
	try {
		const response = await fetch(`${BASE_URL_API}/users/me`, {
			method: 'GET',
			headers: {
				Accept: 'application/json',
				Authorization: `${token}`,
			},
		});
		if (response.ok) {
			const userProfile = await response.json();

			return userProfile;
		} else {
			throw new Error('Failed to fetch user profile');
		}
	} catch (error) {
		console.error('Error:', error.message);
		throw new Error('Failed to fetch user profile');
	}
};

export const fetchCoursesThunk = () => {
	return async (dispatch) => {
		try {
			const token = localStorage.getItem('token');
			if (!token) {
				throw new Error('Token is missing. Please login first.');
			}

			const result = await fetch(`${BASE_URL_API}/courses/all`, {
				method: 'GET',
				headers: {
					Authorization: `${token}`,
					Accept: 'application/json',
				},
			});

			if (!result.ok) {
				const data = await result.json();
				console.error('Failed to get courses:', data.error);
				throw new Error('Failed to get courses');
			}

			const courses = await result.json();
			console.log('Courses fetched successfully:', courses);

			dispatch(coursesSuccess(courses));
		} catch (error) {
			console.error('Unexpected error while fetching courses:', error);
			throw error;
		}
	};
};

export const fetchAuthorsThunk = () => {
	return async (dispatch) => {
		try {
			const token = localStorage.getItem('token');

			if (!token) {
				throw new Error('Token is missing. Please login first.');
			}

			const result = await fetch(`${BASE_URL_API}/authors/all`, {
				method: 'GET',
				headers: {
					Authorization: `${token}`,
					Accept: 'application/json',
				},
			});

			if (!result.ok) {
				const data = await result.json();
				console.error('Failed to get authors:', data.error);
				throw new Error('Failed to get authors');
			}

			const authors = await result.json();
			console.log('Authors fetched successfully:', authors);

			dispatch(authorsSuccess(authors));
		} catch (error) {
			console.error('Unexpected error while fetching authors:', error);
			throw error;
		}
	};
};

export const deleteCourseAPI = async (courseId) => {
	const token = localStorage.getItem('token');
	try {
		const response = await fetch(`${BASE_URL_API}/courses/${courseId}`, {
			method: 'DELETE',
			headers: {
				Authorization: `${token}`,
			},
		});
		if (!response.ok) {
			throw new Error('Failed to delete course');
		}
	} catch (error) {
		throw new Error(error.message);
	}
};

export const fetchCourse = async (courseId, setCourse) => {
	try {
		const response = await fetch(`${BASE_URL_API}/courses/${courseId}`);
		if (!response.ok) {
			throw new Error('Failed to fetch course');
		}
		const courseData = await response.json();
		setCourse(courseData);
	} catch (error) {
		console.error('Error fetching course:', error);
	}
};

export const createCourseAPI = async (courseData) => {
	const token = localStorage.getItem('token');
	try {
		const response = await fetch(`${BASE_URL_API}/courses/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
			body: JSON.stringify(courseData),
		});
		if (!response.ok) {
			throw new Error('Failed to create course');
		}
	} catch (error) {
		console.error('Error creating course:', error);
		throw error;
	}
};

export const updateCourseAPI = async (courseData, courseId) => {
	const token = localStorage.getItem('token');
	try {
		const response = await fetch(`${BASE_URL_API}/courses/${courseId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
			body: JSON.stringify(courseData),
		});
		console.log(response);
		if (!response.ok) {
			throw new Error('Failed to update course');
		}
	} catch (error) {
		console.error('Error updating course:', error);
		throw error;
	}
};

export const createAuthorAPI = async (authorData) => {
	const token = localStorage.getItem('token');
	try {
		const response = await fetch(`${BASE_URL_API}/authors/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `${token}`,
			},
			body: JSON.stringify(authorData),
		});
		if (!response.ok) {
			throw new Error('Failed to create author');
		}
		return await response.json();
	} catch (error) {
		console.error('Error creating author:', error);
		throw error;
	}
};
