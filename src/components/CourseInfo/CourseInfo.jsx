import React, { useState, useEffect } from 'react';
import styles from './CourseInfo.module.css';
import Button from '../../common/Button/Button';
import { transformDateFormat } from '../../helpers/formatCreationDate';
import { getCourseDuration } from '../../helpers/getCourseDuration';
import { useParams, useNavigate } from 'react-router-dom';
import { findCourseAuthors } from '../../helpers/findCourseAuthors';
import { useSelector } from 'react-redux';
import { getCourses } from '../../store/courses/selector';
import { getAuthors } from '../../store/authors/selector';

const CourseInfo = () => {
	const { courseId } = useParams();
	const navigate = useNavigate();
	const [courseInfo, setCourseInfo] = useState({});

	const allCourses = useSelector(getCourses);
	const allAuthors = useSelector(getAuthors);

	useEffect(() => {
		const findCourseById = allCourses.find((course) => courseId === course.id);
		const courseAuthors = findCourseAuthors(allAuthors, findCourseById.authors);
		const courseDuration = getCourseDuration(findCourseById.duration);
		setCourseInfo({
			...findCourseById,
			authors: courseAuthors,
			duration: courseDuration,
		});
	}, [allAuthors, allCourses, courseId]);

	const handleBackButton = () => navigate(-1);

	const { title, description, id, duration, creationDate, authors } =
		courseInfo;
	const { container, infoTitle, aboutTitle, grid, about, info, button } =
		styles;
	const durationHours = duration?.split(' ');

	return (
		<div className={container}>
			<h1>{title}</h1>
			<div className={grid}>
				<h3 className={aboutTitle}>Description:</h3>
				<span className={about}>{description}</span>
				<strong className={infoTitle}>ID:</strong>
				<span className={info}>{id}</span>
				<strong className={infoTitle}>Duration:</strong>
				<div>
					<strong className={info}>{duration && durationHours[0]} </strong>
					<span className={info}>{duration && durationHours[1]}</span>
				</div>
				<strong className={infoTitle}>Created:</strong>
				<span className={info}>{transformDateFormat(creationDate)}</span>
				<strong className={infoTitle}>Authors:</strong>
				<span className={info}>{authors}</span>
			</div>
			<Button
				className={button}
				buttonText={'BACK'}
				onClick={handleBackButton}
			/>
		</div>
	);
};

export default CourseInfo;
