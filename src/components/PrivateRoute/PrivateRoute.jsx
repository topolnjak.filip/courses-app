import { useSelector } from 'react-redux';
import { getRole } from '../../store/user/selector';
import { Navigate } from 'react-router-dom';

export function PrivateRoute({ children }) {
	const role = useSelector(getRole);
	const isAdmin = role === 'admin';

	return isAdmin ? <>{children}</> : <Navigate to='/login' />;
}
