import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import styles from './../Login/Login.module.css';

const Registration = () => {
	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [nameError, setNameError] = useState(false);
	const [emailError, setEmailError] = useState(false);
	const [passwordError, setPasswordError] = useState(false);

	const handleNameChange = (e) => {
		setName(e.target.value);
		setNameError(false);
	};

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		setEmailError(false);
	};

	const handlePasswordChange = (e) => {
		setPassword(e.target.value);
		setPasswordError(false);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (!name) {
			setNameError(true);
			return;
		}
		if (!email) {
			setEmailError(true);
			return;
		}
		if (!password) {
			setPasswordError(true);
			return;
		}

		fetch('http://localhost:4000/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
			},
			body: JSON.stringify({
				name: name,
				password: password,
				email: email,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				console.log('Registration successful:', data);
				navigate('/login');
			})
			.catch((error) => {
				console.error('Error during registration:', error);
			});
	};

	return (
		<>
			<div className={styles.text}>Registration</div>
			<form className={styles.container} onSubmit={handleSubmit}>
				<div className={styles.register}>
					<label className={styles.label}>Name</label>
					<Input
						className={styles.input + (nameError ? ` ${styles.error}` : '')}
						label='Name'
						value={name}
						onChange={handleNameChange}
					/>
					{nameError && (
						<div className={styles.required}>Name is required!</div>
					)}

					<label className={styles.label}>Email</label>
					<Input
						className={styles.input + (emailError ? ` ${styles.error}` : '')}
						label='Email'
						type='email'
						value={email}
						onChange={handleEmailChange}
					/>
					{emailError && (
						<div className={styles.required}>Email is required!</div>
					)}

					<label className={styles.label}>Password</label>
					<Input
						className={styles.input + (passwordError ? ` ${styles.error}` : '')}
						label='Password'
						type='password'
						value={password}
						onChange={handlePasswordChange}
					/>
					{passwordError && (
						<div className={styles.required}>Password is required!</div>
					)}
				</div>
				<div className={styles.login}>
					<Link to='/'>
						<Button
							buttonText={'Registration'}
							className={styles.button}
							onClick={handleSubmit}
						/>
					</Link>
					<div>
						If you have an account you may
						<Link to='/'>
							<Button className={styles.registration} buttonText={'Login'} />
						</Link>
					</div>
				</div>
			</form>
		</>
	);
};

export default Registration;
