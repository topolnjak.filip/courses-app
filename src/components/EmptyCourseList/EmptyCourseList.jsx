import React from 'react';
import styles from './EmptyCourseList.module.css';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getRole } from '../../store/user/selector';

const EmptyCourseList = () => {
	const { container, title, button } = styles;
	const role = useSelector(getRole);
	const isAdmin = role === 'admin';

	return (
		<div className={container}>
			<h1 className={title}>Your List Is Empty</h1>
			{!isAdmin && (
				<p>
					You don't have permissions to create a course. Please log in as ADMIN
				</p>
			)}
			{isAdmin && (
				<Link className={button} to='/courses/add'>
					Add new course
				</Link>
			)}
		</div>
	);
};

export default EmptyCourseList;
