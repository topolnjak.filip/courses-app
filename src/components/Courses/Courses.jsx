import React, { useEffect, useState } from 'react';
import { findCourseAuthors } from '../../helpers/findCourseAuthors';
import CourseCard from './components/CourseCard/CourseCard';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import styles from './Courses.module.css';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getAuthors } from '../../store/authors/selector';
import { getCourses } from '../../store/courses/selector';
import { getRole } from '../../store/user/selector';

const Courses = () => {
	const { container, search, input, button, button_add } = styles;
	const courses = useSelector(getCourses);
	const authors = useSelector(getAuthors);
	const role = useSelector(getRole);
	const [coursesSearch, setCoursesSearch] = useState(courses);
	const [searchQuery, setSearchQuery] = useState('');

	const handleSearch = () => {
		const filteredCourses = courses.filter((course) => {
			const titleMatch = course.title
				.toLowerCase()
				.includes(searchQuery.toLowerCase());
			const idMatch =
				course.id && course.id.toString().includes(searchQuery.toLowerCase());
			return titleMatch || idMatch;
		});
		setCoursesSearch(filteredCourses);
	};

	const onChange = (e) => {
		setSearchQuery(e.target.value);
	};
	const isAdmin = role === 'admin';

	useEffect(() => setCoursesSearch(courses), [courses]);

	return (
		<div className={container}>
			<div className={search}>
				<Input
					className={input}
					type='text'
					value={searchQuery}
					onChange={onChange}
				/>
				<Button
					className={button}
					buttonText={'SEARCH'}
					onClick={handleSearch}
				/>
				{isAdmin && (
					<Link className={button_add} to='/courses/add'>
						Add new course
					</Link>
				)}
			</div>
			{coursesSearch &&
				coursesSearch.map((course) => (
					<CourseCard
						key={course.id}
						course={course}
						authors={findCourseAuthors(authors, course.authors)}
					/>
				))}
		</div>
	);
};

export default Courses;
