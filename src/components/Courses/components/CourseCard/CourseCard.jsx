import styles from './CourseCard.module.css';
import { transformDateFormat } from '../../../../helpers/formatCreationDate';
import { getCourseDuration } from '../../../../helpers/getCourseDuration';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import modify from './media/modify.png';
import trash from './media/trash.png';
import Button from '../../../../common/Button/Button';
import { deleteCourseThunk } from '../../../../store/courses/thunk';
import { getRole } from '../../../../store/user/selector';

const CourseCard = ({ course, authors }) => {
	const formattedDate = transformDateFormat(course.creationDate);
	const formattedDuration = getCourseDuration(course.duration);
	const dispatch = useDispatch();
	const role = useSelector(getRole);

	const handleDelete = async () => {
		try {
			dispatch(deleteCourseThunk(course.id));
		} catch (error) {
			console.error('Error deleting course', error);
		}
	};

	const isAdmin = role === 'admin';

	const {
		card,
		flex_item1,
		flex_item2,
		flex_item3,
		flex_item4,
		h4,
		button,
		button2,
	} = styles;
	return (
		<div className={card}>
			<div className={flex_item1}>
				<h2>{course.title}</h2>
				<p>{course.description}</p>
			</div>
			<div className={flex_item2}>
				<div className={flex_item3}>
					<div className={h4}>Authors:</div>
					<div>{authors}</div>
					<div className={h4}>Duration:</div>
					<div>{formattedDuration}</div>
					<div className={h4}>Created:</div>
					<div>{formattedDate}</div>
				</div>
				<div className={flex_item4}>
					<Link className={button} to={`${course.id}`}>
						SHOW COURSE
					</Link>
					{isAdmin && (
						<>
							<Button
								className={button2}
								imgAlt={'trash'}
								imgSrc={trash}
								onClick={handleDelete}
							></Button>
							<Link to={`/courses/update/${course.id}`} className={button2}>
								<img alt={'modify'} src={modify} />
							</Link>
						</>
					)}
				</div>
			</div>
		</div>
	);
};
CourseCard.propTypes = {
	course: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
	authors: PropTypes.string.isRequired,
};

export default CourseCard;
