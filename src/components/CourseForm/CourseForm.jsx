import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import styles from './CourseForm.module.css';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import AuthorItem from './components/AuthorItem';
import { getCourseDuration } from '../../helpers/getCourseDuration';
import { useDispatch, useSelector } from 'react-redux';
import { getCourses } from '../../store/courses/selector';
import { getAuthors } from '../../store/authors/selector';
import {
	createCourseThunk,
	updateCourseThunk,
} from '../../store/courses/thunk';
import { fetchCourse } from '../../services';
import { createAuthorThunk } from '../../store/authors/thunk';

const CourseForm = () => {
	const {
		container,
		card,
		flex_item1,
		button,
		input_title,
		textarea,
		input,
		buttons,
		ul,
		author_buttons,
		required,
		error,
		course_authors,
	} = styles;

	const navigate = useNavigate();
	const dispatch = useDispatch();
	const courses = useSelector(getCourses);
	const authorsState = useSelector(getAuthors);
	const { courseId } = useParams();
	const [authors, setAuthors] = useState([]);
	const [authorsList, setAuthorsList] = useState([]);
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [course, setCourse] = useState(null);

	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState(0);
	const [authorName, setAuthorName] = useState('');

	const [titleError, setTitleError] = useState(false);
	const [descriptionError, setDescriptionError] = useState(false);
	const [durationError, setDurationError] = useState(false);
	const [authorNameError, setAuthorNameError] = useState(false);

	const courseDuration = getCourseDuration(duration);

	useEffect(() => {
		if (courseId) {
			fetchCourse(courseId, setCourse);
			const courseToUpdate = courses.find((course) => course.id === courseId);
			if (courseToUpdate) {
				setTitle(courseToUpdate.title);
				setDescription(courseToUpdate.description);
				setDuration(courseToUpdate.duration);
				const courseAuthorsData = courseToUpdate.authors.map((authorId) =>
					authorsState.find((author) => author.id === authorId)
				);
				setCourseAuthors(courseAuthorsData);
			}
		}
	}, [courseId, courses, authorsState]);

	const handleTitleChange = (e) => {
		setTitle(e.target.value);
		setTitleError(false);
	};

	const handleDescriptionChange = (e) => {
		setDescription(e.target.value);
		setDescriptionError(false);
	};

	const handleDurationChange = (e) => {
		setDuration(e.target.value);
		setDurationError(false);
	};

	const handleAuthorNameChange = (e) => {
		setAuthorName(e.target.value);
		setAuthorNameError(false);
	};

	const handleCreateAuthor = () => {
		if (authorName.trim() !== '') {
			const newAuthor = {
				name: authorName,
			};
			console.log(newAuthor);
			dispatch(createAuthorThunk(newAuthor));
			setAuthors((prevAuthors) => {
				if (prevAuthors) {
					return [...prevAuthors, newAuthor];
				} else {
					return [newAuthor];
				}
			});
		}
	};

	const handleDeleteAuthor = (authorId) => {
		setAuthors((prevAuthors) =>
			prevAuthors.filter((author) => author.id !== authorId)
		);
	};

	const handleAddAuthor = (author) => {
		const updatedAuthors = authors.filter(({ id }) => id !== author.id);
		setAuthors(updatedAuthors);
		setCourseAuthors([...courseAuthors, author]);
	};

	const handleDeleteCourseAuthor = (authorId) => {
		const deletedAuthor = courseAuthors.find(
			(author) => author.id === authorId
		);
		const updatedCourseAuthors = courseAuthors.filter((a) => a.id !== authorId);
		setCourseAuthors(updatedCourseAuthors);
		if (deletedAuthor) {
			const authorExistsInState = authorsState.some(
				(author) => author.id === deletedAuthor.id
			);
			if (!authorExistsInState) {
				setAuthors([...authors, deletedAuthor]);
			}
			setAuthorsList([...authorsState, deletedAuthor]);
		}
	};
	const durationNumber = parseInt(duration, 10);

	const handleCancel = () => {
		navigate('/courses');
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (!title.trim() || title.trim().length < 2) {
			setTitleError(true);
			return;
		}
		if (!description.trim() || description.trim().length < 2) {
			setDescriptionError(true);
			return;
		}
		if (!duration || duration < 2) {
			setDurationError(true);
			return;
		}
		if (authorName && authorName.trim().length < 2) {
			setAuthorNameError(true);
			return;
		}
		if (
			!titleError &&
			!descriptionError &&
			!durationError &&
			!authorNameError
		) {
			const newCourse = {
				title: title,
				description: description,
				duration: durationNumber,
				authors: courseAuthors.map((author) => author.id),
			};
			dispatch(createCourseThunk(newCourse));
			navigate('/courses');
		}
	};
	const handleUpdate = async (e) => {
		const updatedCourse = {
			title: title,
			description: description,
			duration: durationNumber,
			authors: courseAuthors.map((author) => author.id),
		};
		dispatch(updateCourseThunk(updatedCourse, courseId));
		navigate('/courses');
	};

	return (
		<>
			<form className={container} onSubmit={handleSubmit}>
				<h2>Course Edit/Create Page</h2>
				<div className={card}>
					<div className={flex_item1}>
						<h2>Main Info</h2>
						<label>
							Title
							<Input
								className={input_title + (titleError ? ` ${error}` : '')}
								label='title'
								value={title}
								onChange={handleTitleChange}
							/>
						</label>
						{titleError && <div className={required}>Title is required.</div>}
						<label>
							Description
							<textarea
								className={textarea + (descriptionError ? ` ${error}` : '')}
								name='description'
								value={description}
								onChange={handleDescriptionChange}
							/>
						</label>
						{descriptionError && (
							<div className={required}>Description is required.</div>
						)}
						<h2>Duration</h2>
						<label>
							Duration
							<br></br>
							<Input
								className={input + (durationError ? ` ${error}` : '')}
								label='duration'
								type='number'
								value={duration}
								onChange={handleDurationChange}
								required
							/>
						</label>
						{<span>{courseDuration}</span>}
						{durationError && (
							<div className={required}>Duration is required.</div>
						)}
						<h2>Authors</h2>
						<label>Author Name</label>
						<br></br>
						<Input
							className={input}
							label='authorName'
							value={authorName}
							onChange={handleAuthorNameChange}
						/>
						<Button
							className={button}
							type='button'
							buttonText={'CREATE AUTHOR'}
							onClick={handleCreateAuthor}
						/>

						<div>
							{' '}
							<h4> Author list</h4>{' '}
						</div>

						<ul className={ul}>
							{authorsState.map((author) => (
								<li key={author.id}>
									{
										<AuthorItem
											authorName={author.name}
											className={author_buttons}
											onDelete={() => handleDeleteAuthor(author.id)}
											onAdd={() => handleAddAuthor(author)}
										/>
									}
								</li>
							))}

							{authors.map((author) => (
								<li key={author.id}>
									{
										<AuthorItem
											authorName={author.name}
											className={author_buttons}
											onDelete={() => handleDeleteAuthor(author.id)}
											onAdd={() => handleAddAuthor(author)}
										/>
									}
								</li>
							))}
						</ul>
						{courseAuthors.length > 0 ? (
							<div className={course_authors}>
								<h2>Course Authors</h2>
								<ul className={ul}>
									{courseAuthors.map((author) => (
										<li key={author.id}>
											<AuthorItem
												authorName={author.name}
												className={author_buttons}
												onAdd={() => handleAddAuthor(author)}
												isOnCourseList={courseAuthors.some(
													(courseAuthor) => courseAuthor.id === author.id
												)}
												onDelete={() => handleDeleteCourseAuthor(author.id)}
											/>
										</li>
									))}
								</ul>
							</div>
						) : (
							<div className={course_authors}>
								<h2>Course Authors</h2>
								<p>Author list is empty</p>
							</div>
						)}
					</div>
				</div>
			</form>
			<div className={buttons}>
				<Button
					buttonText={'CANCEL'}
					className={button}
					onClick={handleCancel}
				></Button>
				<Button
					buttonText={courseId ? 'UPDATE COURSE' : 'CREATE COURSE'}
					className={button}
					onClick={courseId ? handleUpdate : handleSubmit}
				></Button>
			</div>
		</>
	);
};

export default CourseForm;
