import React from 'react';
import styles from './AuthorItem.module.css';
import Button from '../../../common/Button/Button';
import PropTypes from 'prop-types';

const AuthorItem = ({
	authorName,
	onDelete,
	onAdd,
	className,
	isOnCourseList,
}) => {
	return (
		<div className={styles.container}>
			<div>{authorName}</div>
			<Button buttonText={'-'} className={className} onClick={onDelete} />
			{!isOnCourseList && (
				<Button buttonText={'+'} className={className} onClick={onAdd} />
			)}
		</div>
	);
};
AuthorItem.defaultProps = {
	isOnCourseList: false,
};

AuthorItem.propTypes = {
	authorName: PropTypes.string.isRequired,
	onDelete: PropTypes.func.isRequired,
	onAdd: PropTypes.func.isRequired,
	className: PropTypes.string.isRequired,
	isOnCourseList: PropTypes.bool.isRequired,
};

export default AuthorItem;
