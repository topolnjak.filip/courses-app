import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import styles from './Login.module.css';
import { fetchCoursesThunk, fetchAuthorsThunk } from '../../services';
import { useDispatch } from 'react-redux';
import { loginUserAndFetchDataThunk } from '../../store/user/thunk';

const Login = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [emailError, setEmailError] = useState(false);
	const [passwordError, setPasswordError] = useState(false);

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		setEmailError(false);
	};

	const handlePasswordChange = (e) => {
		setPassword(e.target.value);
		setPasswordError(false);
	};

	const handleSubmit = async (e) => {
		e.preventDefault();

		if (!email) {
			setEmailError(true);
			return;
		}
		if (!password) {
			setPasswordError(true);
			return;
		}
		try {
			const credentials = { email, password };
			const { loginResponse, userDataResponse } = await dispatch(
				loginUserAndFetchDataThunk(credentials)
			);
			dispatch(fetchCoursesThunk());
			dispatch(fetchAuthorsThunk());

			navigate('/courses');
		} catch (error) {
			console.error('Error', error);
		}
	};

	const {
		text,
		container,
		register,
		label,
		input,
		login,
		button,
		registration,
		registrationText,
		required,
	} = styles;

	return (
		<>
			<div className={text}>Login</div>
			<form className={container} onSubmit={handleSubmit}>
				<div className={register}>
					<label className={styles.label}>Email</label>
					<Input
						className={input + (emailError ? ` ${styles.error}` : '')}
						label='Email'
						type='email'
						value={email}
						onChange={handleEmailChange}
					/>
					{emailError && <div className={required}>Email is required!</div>}
					<label className={label}>Password</label>
					<Input
						className={input + (passwordError ? ` ${styles.error}` : '')}
						label='Password'
						type='password'
						value={password}
						onChange={handlePasswordChange}
					/>
					{passwordError && (
						<div className={required}>Password is required!</div>
					)}
				</div>
				<div className={login}>
					<Button
						buttonText={'Login'}
						className={button}
						onClick={handleSubmit}
					/>
					<div className={registrationText}>
						If you don't have an account you may
					</div>
					<Link to='/registration' className={registration}>
						Registration
					</Link>
				</div>
			</form>
		</>
	);
};

export default Login;
