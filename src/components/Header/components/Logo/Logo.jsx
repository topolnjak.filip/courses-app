import React from 'react';
import logo from './logo.png';

const Logo = () => {
	return (
		<img
			src={logo}
			alt='logo'
			width='100'
			height='auto '
			className='Logo'
		></img>
	);
};

export default Logo;
