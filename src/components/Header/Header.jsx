import Logo from './components/Logo/Logo';
import styles from './Header.module.css';
import Button from '../../common/Button/Button';
import { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { LOGIN_PATH, REGISTRATION_PATH } from './../../constants';
import { useDispatch, useSelector } from 'react-redux';
import { removeUser } from '../../store/user/actions';
import { getUser } from '../../store/user/selector';
import { BASE_URL_API } from '../../services';

const Header = () => {
	const { header, button } = styles;
	const navigate = useNavigate();
	const location = useLocation();
	const dispatch = useDispatch();

	const user = useSelector(getUser);

	useEffect(() => {
		if (
			location.pathname.startsWith('/courses') &&
			!localStorage.getItem('token')
		) {
			navigate(LOGIN_PATH);
		}
	}, [location, navigate]);

	const logout = async () => {
		try {
			const token = localStorage.getItem('token');

			if (!token) {
				throw new Error('Token not found in local storage');
			}

			const response = await fetch(`${BASE_URL_API}/logout`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `${token}`,
				},
			});

			if (!response.ok) {
				throw new Error('Logout request failed');
			}

			dispatch(removeUser());
			console.log('Logout was successful');
			navigate(LOGIN_PATH);
		} catch (error) {
			console.error('Logout failed:', error);
		}
	};

	return (
		<header className={header}>
			<Logo />
			<nav>
				{user && <span>{user} </span>}
				{location.pathname !== LOGIN_PATH &&
					location.pathname !== REGISTRATION_PATH && (
						<Button
							className={button}
							buttonText={'Logout'}
							onClick={logout}
						></Button>
					)}
			</nav>
		</header>
	);
};

export default Header;
