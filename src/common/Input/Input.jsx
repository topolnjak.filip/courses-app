import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ label, type, value, onChange, className }) => {
	return (
		<input
			type={type ? type : 'text'}
			min={type === 'number' ? 0 : null}
			name={label}
			className={className}
			value={value}
			onChange={onChange}
		/>
	);
};

Input.defaultProps = {
	type: '',
	label: '',
};

Input.propTypes = {
	label: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
	onChange: PropTypes.func.isRequired,
	className: PropTypes.string.isRequired,
};

export default Input;
