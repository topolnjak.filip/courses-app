import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ buttonText, className, onClick, type, imgSrc, imgAlt }) => {
	return (
		<button className={className} onClick={onClick} type={type}>
			{imgSrc && <img src={imgSrc} alt={imgAlt} />}
			{buttonText}
		</button>
	);
};

Button.defaultProps = {
	className: '',
	buttonText: '',
	onClick: () => {},
	type: 'button',
};

Button.propTypes = {
	className: PropTypes.string,
	buttonText: PropTypes.string,
	onClick: PropTypes.func,
	type: PropTypes.oneOf(['button', 'submit', 'reset']),
};

export default Button;
