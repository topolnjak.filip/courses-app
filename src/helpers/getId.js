export default function getId(array) {
	return array.map((e) => e.id);
}
