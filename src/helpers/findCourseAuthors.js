export function findCourseAuthors(allAuthors, authorIds) {
	return allAuthors
		.filter((author) => authorIds.includes(author.id))
		.map((author) => author.name)
		.join(', ');
}
