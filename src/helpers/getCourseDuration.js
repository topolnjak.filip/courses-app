export function getCourseDuration(totalMinutes) {
	const hours = Math.floor(totalMinutes / 60);
	const minutes = totalMinutes % 60;

	let formattedHours = hours < 10 ? `0${hours}` : `${hours}`;
	let formattedMinutes = minutes < 10 ? `0${minutes}` : `${minutes}`;

	if (hours === 1) {
		return `${formattedHours}:${formattedMinutes} hour`;
	} else if (minutes === 0) {
		return `${formattedHours}:00 hours`;
	} else {
		return `${formattedHours}:${formattedMinutes} hours`;
	}
}
