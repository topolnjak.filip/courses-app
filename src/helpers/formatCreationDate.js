export function transformDateFormat(inputDate) {
	if (typeof inputDate === 'string') {
		const parts = inputDate.split('/');
		if (parts.length === 3) {
			const month = parts[0].padStart(2, '0');
			const day = parts[1].padStart(2, '0');
			const year = parts[2];
			return `${day}.${month}.${year}`;
		}
	}
	return inputDate;
}
