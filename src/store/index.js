import { configureStore } from '@reduxjs/toolkit';
import { rootReducer } from './rootReducer.js';
import { coursesInitialState } from './courses/reducer.js';
import { authorsInitialState } from './authors/reducer.js';
import { userInitialState } from './user/reducer.js';

const appInitialState = {
	user: userInitialState,
	courses: coursesInitialState,
	authors: authorsInitialState,
};
const store = configureStore(rootReducer);

export default store;
