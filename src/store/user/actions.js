import { LOGIN_SUCCESS, REMOVE_USER, USER_DATA_SUCCESS } from './types';

export const loginSuccess = (payload) => ({
	type: LOGIN_SUCCESS,
	payload,
});
export const userDataSuccess = (payload) => ({
	type: USER_DATA_SUCCESS,
	payload,
});

export const removeUser = () => ({
	type: REMOVE_USER,
});
