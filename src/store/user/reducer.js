import { LOGIN_SUCCESS, REMOVE_USER, USER_DATA_SUCCESS } from './types';

export const userInitialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
	role: '',
};

export const userReducer = (state = userInitialState, action) => {
	switch (action.type) {
		case LOGIN_SUCCESS:
			const nameFromLogin = action.payload.userName;
			return {
				...state,
				isAuth: true,
				name: nameFromLogin === null ? 'Admin' : nameFromLogin,
				token: action.payload.token,
			};
		case USER_DATA_SUCCESS:
			return {
				...state,
				email: action.payload.result.email,
				role: action.payload.result.role,
			};
		case REMOVE_USER:
			return userInitialState;
		default:
			return state;
	}
};
