import { loginSuccess, userDataSuccess } from './actions';
import { getUserData, loginUser } from '../../services';

export const loginUserAndFetchDataThunk = (userData) => {
	return async (dispatch) => {
		try {
			const loginResponse = await loginUser(userData);

			if (loginResponse.token) {
				const userDataResponse = await getUserData(loginResponse.token);

				dispatch(loginSuccess(loginResponse));
				dispatch(userDataSuccess(userDataResponse));

				return { loginResponse, userDataResponse };
			}
		} catch (error) {
			console.error('Unexpected error:', error);
		}
	};
};
