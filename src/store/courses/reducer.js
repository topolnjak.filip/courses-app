import {
	SAVE_COURSES,
	COURSES_SUCCESS,
	DELETE_COURSE,
	UPDATE_COURSE,
} from './types';

export const coursesInitialState = [];

export const coursesReducer = (state = coursesInitialState, action) => {
	switch (action.type) {
		case SAVE_COURSES:
			return [...state, action.payload];
		case UPDATE_COURSE:
			return [...state, action.payload];
		case COURSES_SUCCESS:
			return action.payload.result;
		case DELETE_COURSE:
			return state.filter((course) => course.id !== action.payload.courseId);
		default:
			return state;
	}
};
