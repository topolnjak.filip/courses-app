import {
	deleteCourseAPI,
	createCourseAPI,
	updateCourseAPI,
	fetchCoursesThunk,
} from '../../services';
import { deleteCourse, saveCourse, updateCourse } from './actions';

export const deleteCourseThunk = (courseId) => {
	return async (dispatch) => {
		try {
			await deleteCourseAPI(courseId);

			dispatch(deleteCourse(courseId));

			return courseId;
		} catch (error) {
			console.error('Error deleting course:', error);
			throw error;
		}
	};
};

export const createCourseThunk = (courseData) => {
	return async (dispatch) => {
		try {
			await createCourseAPI(courseData);

			dispatch(saveCourse(courseData));
		} catch (error) {
			console.error('Error creating course:', error);
			throw error;
		}
	};
};

export const updateCourseThunk = (courseData, courseId) => {
	return async (dispatch) => {
		try {
			await updateCourseAPI(courseData, courseId);

			dispatch(updateCourse(courseData));
			dispatch(fetchCoursesThunk());
		} catch (error) {
			console.error('Error creating course:', error);
			throw error;
		}
	};
};
