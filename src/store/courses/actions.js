import {
	DELETE_COURSE,
	SAVE_COURSES,
	COURSES_SUCCESS,
	UPDATE_COURSE,
} from './types';

export const deleteCourse = (courseId) => ({
	type: DELETE_COURSE,
	payload: { courseId },
});
export const saveCourse = (course) => ({
	type: SAVE_COURSES,
	payload: course,
});

export const coursesSuccess = (payload) => ({
	type: COURSES_SUCCESS,
	payload,
});

export const updateCourse = (payload) => ({
	type: UPDATE_COURSE,
	payload,
});
