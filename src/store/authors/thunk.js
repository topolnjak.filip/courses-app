import { saveAuthors } from './actions';
import { createAuthorAPI } from '../../services';

export const createAuthorThunk = (authorData) => {
	return async (dispatch) => {
		try {
			await createAuthorAPI(authorData);

			return dispatch(saveAuthors(authorData));
		} catch (error) {
			console.error('Error creating course:', error);
			throw error;
		}
	};
};
