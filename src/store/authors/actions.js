import { SAVE_AUTHORS, AUTHORS_SUCCESS, DELETE_AUTHOR } from './types';

export const saveAuthors = (payload) => ({
	type: SAVE_AUTHORS,
	payload,
});

export const authorsSuccess = (payload) => ({
	type: AUTHORS_SUCCESS,
	payload,
});

export const deleteAuthor = (payload) => ({
	type: DELETE_AUTHOR,
	payload,
});
