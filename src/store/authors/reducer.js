import { SAVE_AUTHORS, AUTHORS_SUCCESS, DELETE_AUTHOR } from './types';

const authorsInitialState = [];

export const authorsReducer = (state = authorsInitialState, action) => {
	switch (action.type) {
		case SAVE_AUTHORS:
			return [...state, action.payload];
		case AUTHORS_SUCCESS:
			return [...state, ...action.payload.result];
		case DELETE_AUTHOR:
			return state.filter((author) =>
				action.payload.find((b) => b !== author.id)
			);
		default:
			return state;
	}
};
